require 'gem2deb/rake/testtask'
Gem2Deb::Rake::TestTask.new do |t|
  t.libs << "tests"
  t.test_files = FileList['tests/**/*.rb']
end
