#include <string.h>
#include <math.h>
#include "ruby.h"
#include "narray.h"
#include "f2c_minimal.h"

#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define LG(n) ((int)ceil(log((double)(n))/log(2.0)))

extern logical lsame_(char *ca, char *cb);
extern integer ilatrans_(char* trans);
extern integer ilaenv_(integer* ispec, char* name, char* opts, integer* n1, integer* n2, integer* n3, integer* n4);


static VALUE sHelp, sUsage;
static VALUE rblapack_ZERO;

/* for compatibility for NArray and NArray with bigmem patch */
#ifndef NARRAY_BIGMEM
typedef int    na_shape_t;
#endif


