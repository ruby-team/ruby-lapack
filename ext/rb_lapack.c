#include "ruby.h"
#include "rb_lapack.h"

extern void init_lapack_zlanht(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zstemr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zungtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sppcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_syamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dsgesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clar1v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slatrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed9(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_syrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dgetri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgelss(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zbbcsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarzt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsysvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrtri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_lsamen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggsvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeqr2p(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsyconv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slansy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqsb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zung2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgerfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsytri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlatrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csytri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cuncsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slargv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slacon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsytrs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgebak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqr4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_wwaddw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cgehrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clanhe(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaswp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cladiv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zspr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zherfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claset(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorgtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgerqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeql2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggsvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlanhp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilaslr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunbdb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorglq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zheevr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssygvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlatdf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmhr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dppcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dppsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgex2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slag2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarra(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlas2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clar2v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssbevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clasr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlacp2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zungbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chbgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clanhp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstedc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clatrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chseqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cungl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgtrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_xerbla(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ddisna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sladiv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlansy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_lin_berr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_chesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gbamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zhseqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgesvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgerfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasq6(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgerq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarzb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasq1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarcm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasv2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlacpy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slascl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeqpf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpotri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgges(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_porcond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_sgeev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slantb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed6(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cgbrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cpbstf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgerqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgerq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cstemr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlacgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dptts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slange(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssyswapr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqsp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_syrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cgelqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhfrk(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlahqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqr5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgesdd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarnv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpftrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dptsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd8(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssfrk(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_gercond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ctrsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarzb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chegvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggsvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeqp3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sgbrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dsyev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqsp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgebal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cptsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zgbsvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ztgexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_rpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_sgelsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunml2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zupgtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlagtm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqr1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slapmt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtfsm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgegv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqr4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slatrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggglm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrk(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlals0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slags2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chsein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggglm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clasyf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cbdsqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtpcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cptcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ieeeck(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsyevr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clauu2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarfb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clacn2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgesvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgelss(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarft(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clanhf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarfb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sposv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_porfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlaed9(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csytri2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zppequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zcgesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chbev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlapmr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clagtm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chetri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlascl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarfgp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlartg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgesdd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhegv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssyconv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssterf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slahr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slartg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stpttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dppequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_porfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cgetri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_syrcond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_sgebak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slamrg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgecon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaset(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggbak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zuncsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chbevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_gbrcond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_spoequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsygs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_gbrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_cla_syrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ztrsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgelsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaneg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgecon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgetc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqhe(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlauum(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slansb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_icmax1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqr0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgesdd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsyequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_rpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dpftrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaln2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggbak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstedc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgtsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgesdd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgelsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgees(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmhr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgebd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggbak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csytrs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlangt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dposvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sbdsqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgetrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarrk(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpstf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_syamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlanhe(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_gbrcond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_slarf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqge(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytrs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dlascl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zsycon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chbevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhesvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_shseqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgghrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstebz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed8(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlacn2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarft(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggsvp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgges(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgesc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgels(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cptsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zptsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytri2x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlansp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_lin_berr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zgeqp3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlacrm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clals0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqr4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrti2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqp2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgelss(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sppequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sptts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgsvj0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slanst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dporfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strti2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zheequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarfx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpotrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeqp3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zlascl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_dporfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cgeqlf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgelqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqr0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpftrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_syamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlapll(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clantb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zgesvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zgerfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_hercond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cheequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_shgeqz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlasyf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlatps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clanhs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlabad(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spstf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarre(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlansy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgetrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zhesvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlangt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbbrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_ssyrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlat2c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clatps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlatbs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsysv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgtcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd7(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zstein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqsp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasdt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpotrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dlarscl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zgghrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhgeqz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeqrfp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorml2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cposv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cstein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sporfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dtpttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztpcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqr1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dgbrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dpotf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqr1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztpttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsygst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clantr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlansf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqr0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_porcond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dorgbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgsja(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dladiv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spotri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgtrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqgb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasq3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strtri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clacpy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarft(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorbdb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilaslc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstemr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_ssysvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlar2v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zheevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claed8(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgtcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zppcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slatps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorm2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cposvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlaqr4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgecon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgebrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csyrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zptrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtzrzf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsyr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clangb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssbgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dhsein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgelsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_herfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dhseqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlantb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_gerfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dsptrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhecon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgetc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztgevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgels(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cherfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarfg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slar1v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssbgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgetrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_clarscl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_clarf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gercond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cpotri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_porcond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlaev2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_scsum1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_geamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zgbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasq3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgehrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zheev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slapmr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpftrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztgsy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorghr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csycon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zporfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_slaqgb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpotrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorgr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed7(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clanht(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlapmr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chetrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zppsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsyswapr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgels(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dzsum1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlangb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaruv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgsy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgtts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlahef(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgehd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slanv2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggsvp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zgbrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_zgerfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_chptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claic1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gercond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dpteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctpttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlat2s(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlatrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgelsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spotrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlantp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slauu2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqsp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dsysvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zung2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqgb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlange(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slacn2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed6(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhegvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarrr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgsvj0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gerfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ssyrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlantb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cherfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dpftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zposv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sisnan(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarfg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtpttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssyev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_porpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_clauum(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqge(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaev2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarfgp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgetc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dgerfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dbdsdc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clascl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctfttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorghr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sposvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zporfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorg2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_porpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ctrcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsytrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsfrk(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpotrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlagts(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeql2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggsvp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqsb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slartv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgtsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cppsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_slarscl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_sgesvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csrscl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqr5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztfttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpstf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgtrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlartv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_shsein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlansp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlansb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cheevr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slahrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgglse(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgegv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zsysvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_clahqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgex2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlagtm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlagv2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_gbrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_strevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaruv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cheevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaebz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstegr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqhb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssbev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaic1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_lin_berr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dpbstf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunml2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhetri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaebz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsyrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlantp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorcsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlags2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cgesvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_dgesvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_slarnv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstevr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgtcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zupmtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarrj(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsytrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssyevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlagtf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cungr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clacgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssygst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarfb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slagv2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpoequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasv2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarrb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clalsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgelsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slacpy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cbbcsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cporfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cspsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgetrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chptrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgebrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dposv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtfttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtzrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gbrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_slabrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbbrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_geamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlarrv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csyswapr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlascl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarzb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpotf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_porfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_clatdf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhetrs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clahrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chbtrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlanhs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorm2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorml2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csymv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sopgtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlacrt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spotrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zungl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dbdsqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaneg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlanhs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_gbrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cstegr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeqlf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gbrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_clatrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgelsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlapmt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgglse(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zrot(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgesvj(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeqrfp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slagtm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_syrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cgelsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgelsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgelsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chgeqz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clacon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlahrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlapll(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlar1v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeqpf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaln2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorgl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasyf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasq6(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_drscl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarcm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlauu2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilauplo(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggbal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaev2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cspr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpotf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed8(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stzrzf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqhp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_clascl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_chetd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slansp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarzb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpocon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasdt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slapll(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssyevr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaeda(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgtts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpoequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlangb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgges(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaic1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chetrs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunm2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd6(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgees(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_crot(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spotf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlags2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_syrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cgbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlahrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqge(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_porfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlag2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhetd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgsja(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cupmtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlamrg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbbrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgesvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgesvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlapmt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csyconv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dopgtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormhr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgegs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhegs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormhr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgerqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csysvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgees(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormlq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsymv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cupgtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgetf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zstedc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zungql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlantr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sbbcsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgetrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgehd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarfgp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssyevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgegs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgtsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgebd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chetrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlanhf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cspsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorgr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spstrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_strsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorgql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeqrfp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaed8(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clansb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clangt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slar2v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqr0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgtts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csyequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarfx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sstev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarfx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cspmv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claed7(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zstegr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_checon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgehd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpstf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sptcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_chesvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zgelsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgebrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clapmr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhptrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunm2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgegs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_srscl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slapy3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dbbcsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zbdsqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgesc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztfsm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsyconv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpoequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cposvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctzrzf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqsb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarfb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sppsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrtri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssysv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeqrfp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarnv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgsvj1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sptsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqr5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dhgeqz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_classq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slals0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clabrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorgql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstegr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlatzm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgtcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgerqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsytri2x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clag2z(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zherfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zspcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgelq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spftrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunm2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilaclc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sposvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgelq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsyevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarfgp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaset(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytrs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zungr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slantp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsysvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgsvj1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztpttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cungrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_wwaddw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zunglq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasq5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggglm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggglm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlalsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrti2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunglq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaed7(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgelq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaset(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cung2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slartgs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_syrpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlarzt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sgerfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ilaclr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clalsa(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgsy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeqr2p(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssyequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zspsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqhb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_slascl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_dla_porpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_zla_wwaddw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlasd3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgetrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgerq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlahr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gercond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_stfttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chetf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sdisna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgetri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbtrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_iladlc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_iladlr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpoequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilazlr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zppsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slatbs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeqlf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasrt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggsvp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpftri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csytri2x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gercond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_claev2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clansy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggbak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsygv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpotri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zposvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlatrz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpoequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqsb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunbdb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dopmtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgetf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claswp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clantp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasda(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarre(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_heamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zpbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sppsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgexc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgejsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_iladiag(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlatps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhetrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztgex2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorgqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlahqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgerfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrti2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_syrcond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlartgp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunghr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaic1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgesvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarrc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgebak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgetc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarzt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clapmt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarfg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlabrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssygvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chbgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgsy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztgsja(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clartv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slartgp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zspmv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgglse(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slatzm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gbrcond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zgttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_porcond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_sgetrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunghr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgetf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlauum(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgghrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgebal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqhe(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsterf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zlarscl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ztbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeqp3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgevc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgex2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sporfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgelss(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpotrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsyrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stzrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spocon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gbrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ssbevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsyswapr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sbdsdc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_hercond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlaqtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgels(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggsvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilaprec(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctfsm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgebd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gbamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlaqge(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cporfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ssteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clahr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgesc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlartg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsyevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorgtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clahef(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clacp2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasd7(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_porpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_csyrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zhsein(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_syrcond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_chbgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csyr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slalsa(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dgbsvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlassq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorgbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cppcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dptrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlalsa(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgerq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytri2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_hercond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ssygv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasq4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chla_transtype(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztgsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlapy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssbtrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_gbamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cgesv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbstf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhetrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarft(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_heamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_clartg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorgl2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlacpy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorg2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_herfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_slas2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsyequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claed0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctfttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sgesvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_sla_syrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ctpcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgesvj(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgebd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarfg(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cppsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slatdf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaed7(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasdq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dposvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zptsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgetf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dsyrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cpprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilatrans(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsteqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqr5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csytf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtfttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chbgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slahqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssycon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sgbsvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dgbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cungql(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gbrcond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlag2c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztfttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeql2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlargv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zggbal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dppsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlanhb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_rpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_sla_syamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dptsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlatrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaswp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chetrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlanv2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeqlf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slabad(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cspcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlacon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_hercond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cpftrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sptsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_xerbla_array(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slagtf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slangt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slatrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clacrt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgecon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_wwaddw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cpbtf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_gbamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ztgsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlanst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlae2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasdq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbtrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorg2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarzt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlalsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhegvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssptrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpocon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgegv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqhp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctrttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlals0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_gerfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlatbs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqgb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilaver(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgees(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dptcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasq5(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqr1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sspsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dspcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spbstf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_syrcond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cunmr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarfx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgghrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstevr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsytri2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cungbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlartv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_iparmq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cungtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlauu2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stfsm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeqr2p(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztgsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlag2s(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrsen(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd8(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlassq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_herpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlarz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clapll(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlarra(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slauum(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlatdf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpocon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spoequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlacon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cungqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slae2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgebak(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaswp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgesvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stfttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormlq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claqp2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgglse(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgelq2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slassq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgelqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilaenv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgegv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgtsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsbtrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorglq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zptts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlantr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgtrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_claesy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsysv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqp2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_csysvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_slapy2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctpttr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztptri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpgst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_herpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_sgebal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpoequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgbbrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorm2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgelsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csytrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zptcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ilazlc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgtts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctzrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cstedc(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zungrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeesx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slantr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsygvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chegv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_geamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dgesc2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sopmtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarrv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsposv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zdrscl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stpttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorcsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunm2r(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlarnv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgegs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrtri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhpgvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpotrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sormtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ctgsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cgbsvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zhetf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slangb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clatbs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd6(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clacrm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgtsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtrttp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgges(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_gbrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dorgrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaesy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgtsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasq1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cgerfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlasda(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clargv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgeequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgetri(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstemr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrj(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpftrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stgsna(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slag2d(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slagts(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsygvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cunmlq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chesvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlar2v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgetrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clarrv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zspsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmr3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_izmax1(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggbal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasrt(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slansf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clange(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgtsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorgqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrttf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgttrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgebrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sptrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cggev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slalsd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstebz(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeqpf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgehd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztzrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeqr2p(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytri2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zcposv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgbtrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_porcond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dsytrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaqp2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zladiv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasyf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorbdb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhetrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sorgrq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chegs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggrqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_sla_syrcond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_disnan(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_csysv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqsy(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_porcond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_zlabrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_syrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_csytrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gbrcond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dgelqf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlar1v(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgejsv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlatrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cheev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlange(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zungqr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgehrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cheevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgeqrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssygs2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slaqtr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dtgsja(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ztrcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cptts2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dormbr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssysvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssbgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpbrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zla_syrcond_x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_dla_gercond(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ztrsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cppequ(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlansb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_geamv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_sla_gerfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_dlaeda(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgbequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gbrcond_c(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_cpttrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zunmlq(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cpstrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlatrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zpstrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstevx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlargv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dstev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgehrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chegvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_lin_berr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_clatzm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sggev(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slasq4(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slarrr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgesvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zheevd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stpcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlasr(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_cla_gbrfsx_extended(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ztzrzf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stprfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgebal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_zsyrfsx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
#ifdef USEXBLAS
extern void init_lapack_zposvxx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_ssptrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlaed0(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhbgv(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssbgvd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chfrk(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlahr2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlasd2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dpstrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zsytf2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zhegst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlapy3(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsytri2x(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clanhb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dsycon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dggbal(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chptrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_sgtsvx(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cung2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#ifdef USEXBLAS
extern void init_lapack_dla_rpvgrw(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
#endif
extern void init_lapack_strsyl(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zlacn2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chegst(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_chpcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_stbcon(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dgeqpf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clags2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_zgbequb(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlartgs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorg2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlalsa(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaqps(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dorm2l(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_spftrf(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlatzm(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clansp(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_ssytrs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cgeql2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_clatrd(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_cptrfs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_dlaed2(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);
extern void init_lapack_slanhs(VALUE mLapack, VALUE sHelp, VALUE sUsage, VALUE rblapack_ZERO);

void Init_lapack(){
  VALUE mNumRu;
  VALUE mLapack;

  rb_require("narray");

  mNumRu = rb_define_module("NumRu");
  mLapack = rb_define_module_under(mNumRu, "Lapack");

  sHelp = ID2SYM(rb_intern("help"));
  sUsage = ID2SYM(rb_intern("usage"));

  rblapack_ZERO = INT2NUM(0);

  init_lapack_zlanht(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspgst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zstemr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zungtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sppcon(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_syamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dsgesv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clar1v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slatrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed9(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_syrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dgetri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgelss(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zbbcsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarzt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsysvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrtri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_lsamen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormrq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggsvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeqr2p(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsyconv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormql(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slansy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqsb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zung2r(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmrq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgerfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsytri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlatrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csytri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cuncsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slargv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slacon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsytrs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgebak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqr4(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_wwaddw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cgehrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clanhe(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaswp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgsen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cladiv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zspr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zherfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claset(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorgtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgerqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeql2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggsvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlanhp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilaslr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunbdb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorglq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zheevr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssygvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlatdf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmhr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dppcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dppsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgex2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slag2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarra(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlas2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clar2v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssbevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clasr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlacp2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zungbr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chbgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clanhp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstedc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clatrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chseqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cungl2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgtrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhesv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_xerbla(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ddisna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sladiv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlansy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stptri(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_lin_berr(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_chesv(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gbamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zhseqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgesvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgerfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasq6(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgerq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarzb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasq1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarcm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasv2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlacpy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slascl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeqpf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpotri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgges(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_porcond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_sgeev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slantb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed6(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cgbrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cpbstf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgerqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgerq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cstemr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlacgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dptts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slange(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssyswapr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqsp(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_syrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cgelqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhfrk(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlahqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqr5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgesdd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarnv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpftrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dptsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd8(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssfrk(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed5(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_gercond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ctrsen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarzb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chegvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggsvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeqp3(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sgbrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dsyev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqsp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgebal(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cptsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zgbsvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ztgexc(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_rpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_sgelsy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunml2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zupgtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlagtm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqr1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slapmt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtfsm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgegv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqr4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slatrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggglm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrk(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlals0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slags2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chsein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggglm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clasyf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cbdsqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtpcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cptcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ieeeck(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsyevr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clauu2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarfb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clacn2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgesvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormql(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgelss(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarft(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clanhf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarfb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sposv(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_porfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlaed9(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csytri2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zppequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zcgesv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chbev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlapmr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clagtm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chetri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlascl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarfgp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlartg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgesdd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhegv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssyconv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssterf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slahr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slartg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stpttr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dppequ(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_porfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cgetri(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_syrcond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_sgebak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slamrg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgecon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaset(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggbak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zuncsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chbevx(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_gbrcond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_spoequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsygs2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_gbrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_cla_syrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ztrsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgelsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaneg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgecon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgetc2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqhe(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlauum(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slansb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_icmax1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqr0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgesdd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsyequb(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_rpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dpftrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaln2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggbak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstedc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgtsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgesdd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgelsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgees(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmhr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgebd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggbak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csytrs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlangt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dposvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sbdsqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgetrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarrk(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpstf2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_syamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlanhe(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_gbrcond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_slarf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqge(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytrs2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dlascl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zsycon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbgst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chbevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhesvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_shseqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgghrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstebz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed8(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlacn2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarft(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggsvp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgges(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgesc2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgels(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cptsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zptsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytri2x(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlansp(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_lin_berr(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zgeqp3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlacrm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clals0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqr4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrti2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqp2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgelss(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sppequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sptts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgsvj0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slanst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dporfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strti2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zheequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarfx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpotrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeqp3(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zlascl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_dporfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cgeqlf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgelqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqr0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpftrs(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_syamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlapll(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clantb(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zgesvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zgerfs(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_hercond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cheequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_shgeqz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlasyf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmql(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormbr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlatps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clanhs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlabad(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spstf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarre(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlansy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgetrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgesv(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zhesvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlangt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbbrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strcon(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_ssyrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlat2c(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clatps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlatbs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsysv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgtcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgesv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgesv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd7(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zstein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqsp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasdt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpotrs(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dlarscl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zgghrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhgeqz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeqrfp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorml2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cposv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cstein(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sporfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dtpttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztpcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqr1(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dgbrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dpotf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqr1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztpttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsygst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clantr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlansf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqr0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_porcond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dorgbr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgsja(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dladiv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spotri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgtrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqgb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasq3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strtri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clacpy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarft(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorbdb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilaslc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstemr(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_ssysvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlar2v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zheevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claed8(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgtcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zppcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slatps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorm2l(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cposvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlaqr4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgecon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgebrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csyrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zptrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtzrzf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsyr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clangb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssbgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dhsein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgelsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqsy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgsen(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_herfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dhseqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlantb(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_gerfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dsptrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhecon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgetc2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztgevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgels(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cherfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarfg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slar1v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssbgst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgetrf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_clarscl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_clarf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gercond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cpotri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormr2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_porcond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlaev2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_scsum1(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_geamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zgbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasq3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgehrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zheev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slapmr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpftrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztgsy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorghr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csycon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zporfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_slaqgb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpotrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorgr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed7(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clanht(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlapmr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chetrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbgst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zppsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsyswapr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgels(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dzsum1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlangb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaruv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgsy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgtts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlahef(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgehd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slanv2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggsvp(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zgbrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_zgerfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_chptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claic1(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gercond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dpteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctpttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlat2s(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlatrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgelsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spotrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlantp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slauu2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqsp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dsysvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zung2l(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqgb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlange(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slacn2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed6(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhegvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarrr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgsvj0(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gerfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ssyrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgsen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlantb(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cherfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dpftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zposv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sisnan(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarfg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtpttr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssyev(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_porpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_clauum(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqge(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaev2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarfgp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgetc2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dgerfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dbdsdc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clascl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctfttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrsen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorghr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpsv(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sposvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zporfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorg2r(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_porpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ctrcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsytrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsfrk(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpotrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlagts(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeql2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggsvp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqsb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slartv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgtsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cppsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_slarscl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_sgesvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csrscl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqr5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztfttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpstf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgtrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlartv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_shsein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlansp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlansb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cheevr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slahrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgglse(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgegv(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zsysvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_clahqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgex2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlagtm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlagv2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_gbrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_strevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaruv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cheevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaebz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstegr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqhb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssbev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaic1(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_lin_berr(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dpbstf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunml2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhetri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaebz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsyrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlantp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorcsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlags2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cgesvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_dgesvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_slarnv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstevr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgtcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zupmtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarrj(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsytrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssyevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlagtf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cungr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clacgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssygst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarfb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slagv2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpoequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasv2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarrb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clalsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgelsx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slacpy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cbbcsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cporfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cspsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgetrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chptrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgebrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dposv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtfttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtzrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmbr(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gbrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_slabrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbbrd(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_geamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlarrv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csyswapr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlascl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarzb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpotf2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_porfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_clatdf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhetrs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmrq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clahrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chbtrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlanhs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorm2r(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorml2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csymv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sopgtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlacrt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spotrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zungl2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dbdsqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaneg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlanhs(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_gbrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cstegr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeqlf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gbrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_clatrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgelsx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlapmt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgglse(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zrot(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgesvj(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeqrfp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slagtm(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_syrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cgelsy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgelsy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgelsx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chgeqz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clacon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlahrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlapll(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlar1v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeqpf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaln2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorgl2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasyf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasq6(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_drscl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarcm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlauu2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilauplo(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggbal(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaev2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cspr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpotf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed8(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stzrzf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqhp(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_clascl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_chetd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slansp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarzb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpocon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasdt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slapll(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssyevr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqsy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaeda(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgtts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpoequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlangb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgges(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmbr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaic1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chetrs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunm2r(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd6(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgees(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqsy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_crot(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spotf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlags2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_syrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cgbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlahrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqge(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_porfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlag2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhetd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgsja(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cupmtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlamrg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbbrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgesvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgesvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlapmt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csyconv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dopgtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormhr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgegs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhegs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormhr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgerqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csysvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgees(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormlq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsymv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cupgtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgetf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zstedc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zungql(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlantr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sbbcsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgetrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgehd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarfgp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssyevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgegs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgtsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgebd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chetrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlanhf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cspsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorgr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spstrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_strsen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorgql(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeqrfp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaed8(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clansb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clangt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slar2v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqr0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgtts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csyequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarfx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sstev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarfx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cspmv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claed7(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zstegr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_checon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgehd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpstf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sptcon(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_chesvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zgelsx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgebrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clapmr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhptrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunm2l(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgegs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_srscl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slapy3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dbbcsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zbdsqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgesc2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztfsm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsyconv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpoequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cposvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctzrzf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqsb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarfb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sppsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrtri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssysv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeqrfp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspgst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarnv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgsvj1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sptsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqr5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dhgeqz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_classq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slals0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clabrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorgql(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstegr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlatzm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgtcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgerqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsytri2x(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clag2z(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zherfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zspcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgelq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spftrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunm2l(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilaclc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sposvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgelq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsyevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarfgp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaset(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytrs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zungr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slantp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsysvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgsvj1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztpttr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cungrq(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_wwaddw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zunglq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasq5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggglm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggglm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlalsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrti2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunglq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaed7(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgelq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaset(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cung2r(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slartgs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_syrpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlarzt(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sgerfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ilaclr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clalsa(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgsy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeqr2p(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssyequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zspsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqhb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpev(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_slascl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_dla_porpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_zla_wwaddw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlasd3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgetrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgerq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlahr2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gercond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_stfttr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chetf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sdisna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgetri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbtrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_iladlc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_iladlr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpoequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilazlr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zppsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slatbs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeqlf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasrt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggsvp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpftri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csytri2x(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gercond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_claev2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clansy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggbak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsygv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpotri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zposvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlatrz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpoequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqsb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunbdb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dopmtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgetf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claswp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clantp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasda(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarre(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_heamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zpbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sppsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgexc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgejsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_iladiag(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlatps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhetrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztgex2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorgqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlahqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgerfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrti2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_syrcond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlartgp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunghr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaic1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgesvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarrc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgebak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgetc2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarzt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clapmt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarfg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlabrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssygvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chbgst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgsy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztgsja(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clartv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slartgp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstein(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zspmv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgglse(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slatzm(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gbrcond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zgttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_porcond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_sgetrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunghr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgetf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlauum(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgghrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgebal(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqhe(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsterf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zlarscl2(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ztbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeqp3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgevc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgex2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sporfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgelss(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpotrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsyrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stzrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spocon(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gbrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ssbevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsyswapr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sbdsdc(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_hercond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlaqtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgels(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggsvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilaprec(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctfsm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgebd2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gbamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlaqge(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cporfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ssteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clahr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgesc2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlartg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsyevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorgtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clahef(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clacp2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasd7(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_porpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_csyrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zhsein(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_syrcond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_chbgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csyr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slalsa(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dgbsvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlassq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorgbr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cppcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dptrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlalsa(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgerq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytri2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_hercond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ssygv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasq4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chla_transtype(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztgsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlapy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssbtrd(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_gbamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cgesv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbstf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhetrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarft(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_heamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_clartg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorgl2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlacpy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorg2l(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_herfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_slas2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsyequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claed0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctfttr(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sgesvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_sla_syrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ctpcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgesvj(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgebd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarfg(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cppsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slatdf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaed7(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasdq(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dposvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zptsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgetf2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dsyrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cpprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilatrans(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsteqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqr5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csytf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtfttr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chbgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmql(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slahqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssycon(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sgbsvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dgbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cungql(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gbrcond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlag2c(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztfttr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeql2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlargv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zggbal(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dppsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlanhb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_rpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_sla_syamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dptsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlatrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaswp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chetrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlanv2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeqlf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slabad(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cspcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpgst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlacon(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_hercond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cpftrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sptsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_xerbla_array(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slagtf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slangt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slatrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clacrt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgecon(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_wwaddw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cpbtf2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_gbamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ztgsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlanst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlae2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasdq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbtrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorg2r(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarzt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlalsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhegvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssptrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpocon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgegv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqhp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctrttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlals0(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_gerfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlatbs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqgb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilaver(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgees(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dptcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasq5(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqr1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sspsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dspcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spbstf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_syrcond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cunmr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarfx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgghrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstevr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsytri2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cungbr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlartv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_iparmq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cungtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormrq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlauu2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stfsm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeqr2p(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztgsen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlag2s(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrsen(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd8(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlassq(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_herpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlarz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clapll(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlarra(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slauum(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlatdf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpocon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spoequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlacon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cungqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slae2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgebak(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaswp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgesvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stfttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormlq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claqp2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgglse(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgelq2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slassq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgelqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilaenv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgegv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgtsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsbtrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorglq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zptts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlantr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgtrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_claesy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsysv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqp2(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_csysvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_slapy2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctpttr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztptri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpgst(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_herpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_sgebal(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpoequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgbbrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorm2r(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgelsy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csytrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zptcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ilazlc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgtts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctzrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cstedc(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zungrq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeesx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slantr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsygvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chegv(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_geamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dgesc2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sopmtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarrv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsposv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zdrscl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stpttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorcsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunm2r(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlarnv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgegs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrtri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhpgvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpotrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sormtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ctgsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmqr(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cgbsvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zhetf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slangb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clatbs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd6(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clacrm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgtsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtrttp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgges(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_gbrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dorgrq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaesy(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgtsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasq1(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cgerfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlasda(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clargv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgeequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgetri(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstemr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrj(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpftrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stgsna(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slag2d(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slagts(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsygvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cunmlq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chesvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlar2v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgetrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clarrv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zspsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmr3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_izmax1(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggbal(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasrt(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slansf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clange(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgtsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorgqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrttf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgttrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgebrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sptrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cggev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slalsd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstebz(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeqpf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgehd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztzrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeqr2p(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytri2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zcposv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgbtrs(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_porcond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dsytrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaqp2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zladiv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasyf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorbdb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhetrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sorgrq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chegs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggrqf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_sla_syrcond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_disnan(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_csysv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqsy(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_porcond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_zlabrd(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_syrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_csytrf(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gbrcond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dgelqf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlar1v(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgejsv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlatrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cheev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlange(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zungqr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgehrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cheevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgeqrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssygs2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slaqtr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dtgsja(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ztrcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cptts2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dormbr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssysvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssbgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpbrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zla_syrcond_x(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_dla_gercond(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ztrsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cppequ(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlansb(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_geamv(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_sla_gerfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_dlaeda(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgbequb(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gbrcond_c(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_cpttrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zunmlq(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cpstrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlatrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zpstrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstevx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlargv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dstev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgehrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chegvx(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_lin_berr(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_clatzm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sggev(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slasq4(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slarrr(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgesvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zheevd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stpcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlasr(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_cla_gbrfsx_extended(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ztzrzf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stprfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgebal(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_zsyrfsx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
#ifdef USEXBLAS
  init_lapack_zposvxx(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_ssptrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlaed0(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhbgv(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssbgvd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chfrk(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlahr2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlasd2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dpstrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zsytf2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zhegst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlapy3(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsytri2x(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clanhb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dsycon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dggbal(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chptrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_sgtsvx(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cung2l(mLapack, sHelp, sUsage, rblapack_ZERO);
#ifdef USEXBLAS
  init_lapack_dla_rpvgrw(mLapack, sHelp, sUsage, rblapack_ZERO);
#endif
  init_lapack_strsyl(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zlacn2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chegst(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_chpcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_stbcon(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dgeqpf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clags2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_zgbequb(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlartgs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorg2l(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlalsa(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaqps(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dorm2l(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_spftrf(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlatzm(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clansp(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_ssytrs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cgeql2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_clatrd(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_cptrfs(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_dlaed2(mLapack, sHelp, sUsage, rblapack_ZERO);
  init_lapack_slanhs(mLapack, sHelp, sUsage, rblapack_ZERO);
}
