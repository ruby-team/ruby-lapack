#include "rb_lapack.h"

extern VOID claein_(logical* rightv, logical* noinit, integer* n, complex* h, integer* ldh, complex* w, complex* v, complex* b, integer* ldb, real* rwork, real* eps3, real* smlnum, integer* info);


static VALUE
rblapack_claein(int argc, VALUE *argv, VALUE self){
  VALUE rblapack_rightv;
  logical rightv; 
  VALUE rblapack_noinit;
  logical noinit; 
  VALUE rblapack_h;
  complex *h; 
  VALUE rblapack_w;
  complex w; 
  VALUE rblapack_v;
  complex *v; 
  VALUE rblapack_eps3;
  real eps3; 
  VALUE rblapack_smlnum;
  real smlnum; 
  VALUE rblapack_info;
  integer info; 
  VALUE rblapack_v_out__;
  complex *v_out__;
  complex *b;
  real *rwork;

  integer ldh;
  integer n;
  integer ldb;

  VALUE rblapack_options;
  if (argc > 0 && TYPE(argv[argc-1]) == T_HASH) {
    argc--;
    rblapack_options = argv[argc];
    if (rb_hash_aref(rblapack_options, sHelp) == Qtrue) {
      printf("%s\n", "USAGE:\n  info, v = NumRu::Lapack.claein( rightv, noinit, h, w, v, eps3, smlnum, [:usage => usage, :help => help])\n\n\nFORTRAN MANUAL\n      SUBROUTINE CLAEIN( RIGHTV, NOINIT, N, H, LDH, W, V, B, LDB, RWORK, EPS3, SMLNUM, INFO )\n\n*  Purpose\n*  =======\n*\n*  CLAEIN uses inverse iteration to find a right or left eigenvector\n*  corresponding to the eigenvalue W of a complex upper Hessenberg\n*  matrix H.\n*\n\n*  Arguments\n*  =========\n*\n*  RIGHTV   (input) LOGICAL\n*          = .TRUE. : compute right eigenvector;\n*          = .FALSE.: compute left eigenvector.\n*\n*  NOINIT   (input) LOGICAL\n*          = .TRUE. : no initial vector supplied in V\n*          = .FALSE.: initial vector supplied in V.\n*\n*  N       (input) INTEGER\n*          The order of the matrix H.  N >= 0.\n*\n*  H       (input) COMPLEX array, dimension (LDH,N)\n*          The upper Hessenberg matrix H.\n*\n*  LDH     (input) INTEGER\n*          The leading dimension of the array H.  LDH >= max(1,N).\n*\n*  W       (input) COMPLEX\n*          The eigenvalue of H whose corresponding right or left\n*          eigenvector is to be computed.\n*\n*  V       (input/output) COMPLEX array, dimension (N)\n*          On entry, if NOINIT = .FALSE., V must contain a starting\n*          vector for inverse iteration; otherwise V need not be set.\n*          On exit, V contains the computed eigenvector, normalized so\n*          that the component of largest magnitude has magnitude 1; here\n*          the magnitude of a complex number (x,y) is taken to be\n*          |x| + |y|.\n*\n*  B       (workspace) COMPLEX array, dimension (LDB,N)\n*\n*  LDB     (input) INTEGER\n*          The leading dimension of the array B.  LDB >= max(1,N).\n*\n*  RWORK   (workspace) REAL array, dimension (N)\n*\n*  EPS3    (input) REAL\n*          A small machine-dependent value which is used to perturb\n*          close eigenvalues, and to replace zero pivots.\n*\n*  SMLNUM  (input) REAL\n*          A machine-dependent value close to the underflow threshold.\n*\n*  INFO    (output) INTEGER\n*          = 0:  successful exit\n*          = 1:  inverse iteration did not converge; V is set to the\n*                last iterate.\n*\n\n*  =====================================================================\n*\n\n");
      return Qnil;
    }
    if (rb_hash_aref(rblapack_options, sUsage) == Qtrue) {
      printf("%s\n", "USAGE:\n  info, v = NumRu::Lapack.claein( rightv, noinit, h, w, v, eps3, smlnum, [:usage => usage, :help => help])\n");
      return Qnil;
    } 
  } else
    rblapack_options = Qnil;
  if (argc != 7 && argc != 7)
    rb_raise(rb_eArgError,"wrong number of arguments (%d for 7)", argc);
  rblapack_rightv = argv[0];
  rblapack_noinit = argv[1];
  rblapack_h = argv[2];
  rblapack_w = argv[3];
  rblapack_v = argv[4];
  rblapack_eps3 = argv[5];
  rblapack_smlnum = argv[6];
  if (argc == 7) {
  } else if (rblapack_options != Qnil) {
  } else {
  }

  rightv = (rblapack_rightv == Qtrue);
  if (!NA_IsNArray(rblapack_h))
    rb_raise(rb_eArgError, "h (3th argument) must be NArray");
  if (NA_RANK(rblapack_h) != 2)
    rb_raise(rb_eArgError, "rank of h (3th argument) must be %d", 2);
  ldh = NA_SHAPE0(rblapack_h);
  n = NA_SHAPE1(rblapack_h);
  if (NA_TYPE(rblapack_h) != NA_SCOMPLEX)
    rblapack_h = na_change_type(rblapack_h, NA_SCOMPLEX);
  h = NA_PTR_TYPE(rblapack_h, complex*);
  if (!NA_IsNArray(rblapack_v))
    rb_raise(rb_eArgError, "v (5th argument) must be NArray");
  if (NA_RANK(rblapack_v) != 1)
    rb_raise(rb_eArgError, "rank of v (5th argument) must be %d", 1);
  if (NA_SHAPE0(rblapack_v) != n)
    rb_raise(rb_eRuntimeError, "shape 0 of v must be the same as shape 1 of h");
  if (NA_TYPE(rblapack_v) != NA_SCOMPLEX)
    rblapack_v = na_change_type(rblapack_v, NA_SCOMPLEX);
  v = NA_PTR_TYPE(rblapack_v, complex*);
  smlnum = (real)NUM2DBL(rblapack_smlnum);
  noinit = (rblapack_noinit == Qtrue);
  eps3 = (real)NUM2DBL(rblapack_eps3);
  w.r = (real)NUM2DBL(rb_funcall(rblapack_w, rb_intern("real"), 0));
  w.i = (real)NUM2DBL(rb_funcall(rblapack_w, rb_intern("imag"), 0));
  ldb = MAX(1,n);
  {
    na_shape_t shape[1];
    shape[0] = n;
    rblapack_v_out__ = na_make_object(NA_SCOMPLEX, 1, shape, cNArray);
  }
  v_out__ = NA_PTR_TYPE(rblapack_v_out__, complex*);
  MEMCPY(v_out__, v, complex, NA_TOTAL(rblapack_v));
  rblapack_v = rblapack_v_out__;
  v = v_out__;
  b = ALLOC_N(complex, (ldb)*(n));
  rwork = ALLOC_N(real, (n));

  claein_(&rightv, &noinit, &n, h, &ldh, &w, v, b, &ldb, rwork, &eps3, &smlnum, &info);

  free(b);
  free(rwork);
  rblapack_info = INT2NUM(info);
  return rb_ary_new3(2, rblapack_info, rblapack_v);
}

void
init_lapack_claein(VALUE mLapack, VALUE sH, VALUE sU, VALUE zero){
  sHelp = sH;
  sUsage = sU;
  rblapack_ZERO = zero;

  rb_define_module_function(mLapack, "claein", rblapack_claein, -1);
}
