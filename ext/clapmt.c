#include "rb_lapack.h"

extern VOID clapmt_(logical* forwrd, integer* m, integer* n, complex* x, integer* ldx, integer* k);


static VALUE
rblapack_clapmt(int argc, VALUE *argv, VALUE self){
  VALUE rblapack_forwrd;
  logical forwrd; 
  VALUE rblapack_m;
  integer m; 
  VALUE rblapack_x;
  complex *x; 
  VALUE rblapack_k;
  integer *k; 
  VALUE rblapack_x_out__;
  complex *x_out__;
  VALUE rblapack_k_out__;
  integer *k_out__;

  integer ldx;
  integer n;

  VALUE rblapack_options;
  if (argc > 0 && TYPE(argv[argc-1]) == T_HASH) {
    argc--;
    rblapack_options = argv[argc];
    if (rb_hash_aref(rblapack_options, sHelp) == Qtrue) {
      printf("%s\n", "USAGE:\n  x, k = NumRu::Lapack.clapmt( forwrd, m, x, k, [:usage => usage, :help => help])\n\n\nFORTRAN MANUAL\n      SUBROUTINE CLAPMT( FORWRD, M, N, X, LDX, K )\n\n*  Purpose\n*  =======\n*\n*  CLAPMT rearranges the columns of the M by N matrix X as specified\n*  by the permutation K(1),K(2),...,K(N) of the integers 1,...,N.\n*  If FORWRD = .TRUE.,  forward permutation:\n*\n*       X(*,K(J)) is moved X(*,J) for J = 1,2,...,N.\n*\n*  If FORWRD = .FALSE., backward permutation:\n*\n*       X(*,J) is moved to X(*,K(J)) for J = 1,2,...,N.\n*\n\n*  Arguments\n*  =========\n*\n*  FORWRD  (input) LOGICAL\n*          = .TRUE., forward permutation\n*          = .FALSE., backward permutation\n*\n*  M       (input) INTEGER\n*          The number of rows of the matrix X. M >= 0.\n*\n*  N       (input) INTEGER\n*          The number of columns of the matrix X. N >= 0.\n*\n*  X       (input/output) COMPLEX array, dimension (LDX,N)\n*          On entry, the M by N matrix X.\n*          On exit, X contains the permuted matrix X.\n*\n*  LDX     (input) INTEGER\n*          The leading dimension of the array X, LDX >= MAX(1,M).\n*\n*  K       (input/output) INTEGER array, dimension (N)\n*          On entry, K contains the permutation vector. K is used as\n*          internal workspace, but reset to its original value on\n*          output.\n*\n\n*  =====================================================================\n*\n*     .. Local Scalars ..\n      INTEGER            I, II, J, IN\n      COMPLEX            TEMP\n*     ..\n\n");
      return Qnil;
    }
    if (rb_hash_aref(rblapack_options, sUsage) == Qtrue) {
      printf("%s\n", "USAGE:\n  x, k = NumRu::Lapack.clapmt( forwrd, m, x, k, [:usage => usage, :help => help])\n");
      return Qnil;
    } 
  } else
    rblapack_options = Qnil;
  if (argc != 4 && argc != 4)
    rb_raise(rb_eArgError,"wrong number of arguments (%d for 4)", argc);
  rblapack_forwrd = argv[0];
  rblapack_m = argv[1];
  rblapack_x = argv[2];
  rblapack_k = argv[3];
  if (argc == 4) {
  } else if (rblapack_options != Qnil) {
  } else {
  }

  forwrd = (rblapack_forwrd == Qtrue);
  if (!NA_IsNArray(rblapack_x))
    rb_raise(rb_eArgError, "x (3th argument) must be NArray");
  if (NA_RANK(rblapack_x) != 2)
    rb_raise(rb_eArgError, "rank of x (3th argument) must be %d", 2);
  ldx = NA_SHAPE0(rblapack_x);
  n = NA_SHAPE1(rblapack_x);
  if (NA_TYPE(rblapack_x) != NA_SCOMPLEX)
    rblapack_x = na_change_type(rblapack_x, NA_SCOMPLEX);
  x = NA_PTR_TYPE(rblapack_x, complex*);
  m = NUM2INT(rblapack_m);
  if (!NA_IsNArray(rblapack_k))
    rb_raise(rb_eArgError, "k (4th argument) must be NArray");
  if (NA_RANK(rblapack_k) != 1)
    rb_raise(rb_eArgError, "rank of k (4th argument) must be %d", 1);
  if (NA_SHAPE0(rblapack_k) != n)
    rb_raise(rb_eRuntimeError, "shape 0 of k must be the same as shape 1 of x");
  if (NA_TYPE(rblapack_k) != NA_LINT)
    rblapack_k = na_change_type(rblapack_k, NA_LINT);
  k = NA_PTR_TYPE(rblapack_k, integer*);
  {
    na_shape_t shape[2];
    shape[0] = ldx;
    shape[1] = n;
    rblapack_x_out__ = na_make_object(NA_SCOMPLEX, 2, shape, cNArray);
  }
  x_out__ = NA_PTR_TYPE(rblapack_x_out__, complex*);
  MEMCPY(x_out__, x, complex, NA_TOTAL(rblapack_x));
  rblapack_x = rblapack_x_out__;
  x = x_out__;
  {
    na_shape_t shape[1];
    shape[0] = n;
    rblapack_k_out__ = na_make_object(NA_LINT, 1, shape, cNArray);
  }
  k_out__ = NA_PTR_TYPE(rblapack_k_out__, integer*);
  MEMCPY(k_out__, k, integer, NA_TOTAL(rblapack_k));
  rblapack_k = rblapack_k_out__;
  k = k_out__;

  clapmt_(&forwrd, &m, &n, x, &ldx, k);

  return rb_ary_new3(2, rblapack_x, rblapack_k);
}

void
init_lapack_clapmt(VALUE mLapack, VALUE sH, VALUE sU, VALUE zero){
  sHelp = sH;
  sUsage = sU;
  rblapack_ZERO = zero;

  rb_define_module_function(mLapack, "clapmt", rblapack_clapmt, -1);
}
