#include "rb_lapack.h"

extern VOID srscl_(integer* n, real* sa, real* sx, integer* incx);


static VALUE
rblapack_srscl(int argc, VALUE *argv, VALUE self){
  VALUE rblapack_n;
  integer n; 
  VALUE rblapack_sa;
  real sa; 
  VALUE rblapack_sx;
  real *sx; 
  VALUE rblapack_incx;
  integer incx; 
  VALUE rblapack_sx_out__;
  real *sx_out__;


  VALUE rblapack_options;
  if (argc > 0 && TYPE(argv[argc-1]) == T_HASH) {
    argc--;
    rblapack_options = argv[argc];
    if (rb_hash_aref(rblapack_options, sHelp) == Qtrue) {
      printf("%s\n", "USAGE:\n  sx = NumRu::Lapack.srscl( n, sa, sx, incx, [:usage => usage, :help => help])\n\n\nFORTRAN MANUAL\n      SUBROUTINE SRSCL( N, SA, SX, INCX )\n\n*  Purpose\n*  =======\n*\n*  SRSCL multiplies an n-element real vector x by the real scalar 1/a.\n*  This is done without overflow or underflow as long as\n*  the final result x/a does not overflow or underflow.\n*\n\n*  Arguments\n*  =========\n*\n*  N       (input) INTEGER\n*          The number of components of the vector x.\n*\n*  SA      (input) REAL\n*          The scalar a which is used to divide each component of x.\n*          SA must be >= 0, or the subroutine will divide by zero.\n*\n*  SX      (input/output) REAL array, dimension\n*                         (1+(N-1)*abs(INCX))\n*          The n-element vector x.\n*\n*  INCX    (input) INTEGER\n*          The increment between successive values of the vector SX.\n*          > 0:  SX(1) = X(1) and SX(1+(i-1)*INCX) = x(i),     1< i<= n\n*\n\n* =====================================================================\n*\n\n");
      return Qnil;
    }
    if (rb_hash_aref(rblapack_options, sUsage) == Qtrue) {
      printf("%s\n", "USAGE:\n  sx = NumRu::Lapack.srscl( n, sa, sx, incx, [:usage => usage, :help => help])\n");
      return Qnil;
    } 
  } else
    rblapack_options = Qnil;
  if (argc != 4 && argc != 4)
    rb_raise(rb_eArgError,"wrong number of arguments (%d for 4)", argc);
  rblapack_n = argv[0];
  rblapack_sa = argv[1];
  rblapack_sx = argv[2];
  rblapack_incx = argv[3];
  if (argc == 4) {
  } else if (rblapack_options != Qnil) {
  } else {
  }

  n = NUM2INT(rblapack_n);
  incx = NUM2INT(rblapack_incx);
  sa = (real)NUM2DBL(rblapack_sa);
  if (!NA_IsNArray(rblapack_sx))
    rb_raise(rb_eArgError, "sx (3th argument) must be NArray");
  if (NA_RANK(rblapack_sx) != 1)
    rb_raise(rb_eArgError, "rank of sx (3th argument) must be %d", 1);
  if (NA_SHAPE0(rblapack_sx) != (1+(n-1)*abs(incx)))
    rb_raise(rb_eRuntimeError, "shape 0 of sx must be %d", 1+(n-1)*abs(incx));
  if (NA_TYPE(rblapack_sx) != NA_SFLOAT)
    rblapack_sx = na_change_type(rblapack_sx, NA_SFLOAT);
  sx = NA_PTR_TYPE(rblapack_sx, real*);
  {
    na_shape_t shape[1];
    shape[0] = 1+(n-1)*abs(incx);
    rblapack_sx_out__ = na_make_object(NA_SFLOAT, 1, shape, cNArray);
  }
  sx_out__ = NA_PTR_TYPE(rblapack_sx_out__, real*);
  MEMCPY(sx_out__, sx, real, NA_TOTAL(rblapack_sx));
  rblapack_sx = rblapack_sx_out__;
  sx = sx_out__;

  srscl_(&n, &sa, sx, &incx);

  return rblapack_sx;
}

void
init_lapack_srscl(VALUE mLapack, VALUE sH, VALUE sU, VALUE zero){
  sHelp = sH;
  sUsage = sU;
  rblapack_ZERO = zero;

  rb_define_module_function(mLapack, "srscl", rblapack_srscl, -1);
}
